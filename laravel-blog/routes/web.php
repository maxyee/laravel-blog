<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Authentication Router
/*
Route::get('auth/login', 'Auth\LoginController@getIndex');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/login', 'Auth\AuthController@getLogout');
*/
//Registration Routes
/*
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
*/

//Password Reset Routes
Route::get('password/resetting/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/resetting', 'Auth\PasswordController@reset');

Route::get('/contact','PagesController@getContact');
Route::get('/about','PagesController@getAbout');
Route::get('/','PagesController@getIndex');



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('posts', 'PostController');

Route::get('blog/{slug}', ['as' => 'blog.single','uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
